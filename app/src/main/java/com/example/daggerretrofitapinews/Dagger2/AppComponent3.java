package com.example.daggerretrofitapinews.Dagger2;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;


@Component(modules = {AndroidSupportInjectionModule.class,FragmentModule.class,MyModule.class})
@Singleton
public interface AppComponent3 extends AndroidInjector<DaggerApplication> {
    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder applications(Application application);
        AppComponent3 builders();
    }
}
