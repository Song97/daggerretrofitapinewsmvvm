package com.example.daggerretrofitapinews.Dagger2;

import androidx.lifecycle.ViewModelProvider;

import com.example.daggerretrofitapinews.MVVM.DashBoardViewModelFactory;
import com.example.daggerretrofitapinews.Retorfit.BaseUrl;
import com.example.daggerretrofitapinews.Retorfit.ServiceApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class MyModule {

    @Provides
    Retrofit getRetrofit(){
        Retrofit  retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    ServiceApi getServiceApi(){
        ServiceApi  serviceApi = getRetrofit().create(ServiceApi.class);
        return serviceApi;
    }

    @Provides
    DashBoardViewModelFactory getFactory(){
        return new DashBoardViewModelFactory(getServiceApi());
    }


}
