package com.example.daggerretrofitapinews;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

public class DetailInformation extends AppCompatActivity {

    TextView title;
    TextView author;
    TextView content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_information);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         author = findViewById(R.id.D_author);
         title = findViewById(R.id.D_Title);
        TextView time = findViewById(R.id.D_Time);
         content = findViewById(R.id.D_Content);
        ImageView img1 = findViewById(R.id.D_imageView);


        Intent intent = getIntent();
        author.setText(intent.getStringExtra("author"));
        title.setText(intent.getStringExtra("title"));
        time.setText(intent.getStringExtra("time"));
        content.setText(intent.getStringExtra("content"));

        Glide.with(this)
                .load(intent.getStringExtra("img"))
                .into(img1);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_share:
                System.out.println("Click share");
                shareInformation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_share_information,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void shareInformation(){
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        //email
        intent.putExtra(Intent.EXTRA_SUBJECT,title.getText());
        intent.putExtra(Intent.EXTRA_TEXT,content.getText());
        //package messager

        startActivity(Intent.createChooser(intent,"Share Via"));
    }
}
