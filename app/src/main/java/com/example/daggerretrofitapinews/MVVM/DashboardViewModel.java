package com.example.daggerretrofitapinews.MVVM;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.daggerretrofitapinews.Retorfit.Entity.AllArticles;
import com.example.daggerretrofitapinews.Retorfit.Entity.Article;
import com.example.daggerretrofitapinews.Retorfit.ServiceApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends ViewModel {

    private MutableLiveData<AllArticles<Article>> data = new MutableLiveData<>();
    private ServiceApi api;
    public DashboardViewModel(ServiceApi api) {
        this.api = api;
    }

    public LiveData<AllArticles<Article>> getAllArticle(){

        Call<AllArticles<Article>> call = api.getAllArticleNoParam();
        call.enqueue(new Callback<AllArticles<Article>>() {
            @Override
            public void onResponse(Call<AllArticles<Article>> call, Response<AllArticles<Article>> response) {
                System.out.println(response.code());
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<AllArticles<Article>> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }


}
