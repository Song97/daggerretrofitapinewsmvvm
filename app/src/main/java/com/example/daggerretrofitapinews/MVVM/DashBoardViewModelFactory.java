package com.example.daggerretrofitapinews.MVVM;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.daggerretrofitapinews.Retorfit.ServiceApi;

public class DashBoardViewModelFactory implements ViewModelProvider.Factory {
    private ServiceApi api;
    public DashBoardViewModelFactory(ServiceApi serviceApi) {
        this.api = serviceApi;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(DashboardViewModel.class)){
            return (T) new DashboardViewModel(api);
        }else{
            throw new IllegalArgumentException("Unknown model class");
        }

    }
}
