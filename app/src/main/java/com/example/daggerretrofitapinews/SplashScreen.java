package com.example.daggerretrofitapinews;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.Wave;

import in.codeshuffle.typewriterview.TypeWriterView;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        ProgressBar progressBar[] = {findViewById(R.id.spin_kit1),findViewById(R.id.spin_kit2)};
        progressBar[0].setIndeterminateDrawable(new Wave());
        progressBar[1].setIndeterminateDrawable(new Wave());

        //Create Object and refer to layout view
       final TypeWriterView typeWriterView=findViewById(R.id.typeWriterView);

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    //Setting each character animation delay
                    typeWriterView.setDelay(100);
                    //Setting music effect On/Off
                    typeWriterView.setWithMusic(true);
                    //Animating Text
                    typeWriterView.animateText("Hello My Name is Akon I am from African IDOL...!");
                    sleep(5*1000);
                    Intent i=new Intent(getBaseContext(),MMainActivity.class);
                    startActivity(i);

                    //Remove activity
                    finish();
                } catch (Exception e) { }
            }
        };
        // start thread
        background.start();
    }
}
