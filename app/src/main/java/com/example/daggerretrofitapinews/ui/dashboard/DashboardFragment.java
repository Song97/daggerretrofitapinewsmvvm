package com.example.daggerretrofitapinews.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.daggerretrofitapinews.DetailInformation;
import com.example.daggerretrofitapinews.MVVM.DashBoardViewModelFactory;
import com.example.daggerretrofitapinews.MVVM.DashboardViewModel;
import com.example.daggerretrofitapinews.ModelAdapters.Adapter.NewAdapter;
import com.example.daggerretrofitapinews.ModelAdapters.MyListener;
import com.example.daggerretrofitapinews.R;
import com.example.daggerretrofitapinews.Retorfit.Entity.AllArticles;
import com.example.daggerretrofitapinews.Retorfit.Entity.Article;
import com.github.ybq.android.spinkit.style.WanderingCubes;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class DashboardFragment extends DaggerFragment implements MyListener {

    private RecyclerView recyclerView;
    private List<Article> articleList;
    private NewAdapter adapter;
    private ProgressBar progressBar;
    @Inject
    DashBoardViewModelFactory factory;
    private DashboardViewModel dashboardViewModel;

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context  = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        recyclerView = root.findViewById(R.id.rvDashboard);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext(),LinearLayoutManager.VERTICAL,false));

        progressBar = root.findViewById(R.id.spin_kit);
        progressBar.setIndeterminateDrawable(new WanderingCubes());


        dashboardViewModel  = ViewModelProviders.of(this,factory).get(DashboardViewModel.class);
        dashboardViewModel.getAllArticle().observe(this, new Observer<AllArticles<Article>>() {
            @Override
            public void onChanged(AllArticles<Article> articleAllArticles) {
                articleList = new ArrayList<>(articleAllArticles.getArticles());
                adapter = new NewAdapter(root.getContext(),articleList);
                recyclerView.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
                adapter.setClickMyLisener(DashboardFragment.this);

            }
        });

        return root;
    }

    @Override
    public void myClickListener(int p) {

        Intent intent = new Intent(context, DetailInformation.class);
        intent.putExtra("img",articleList.get(p).getUrlToImage());
        intent.putExtra("author",articleList.get(p).getAuthor());
        intent.putExtra("title",articleList.get(p).getTitle());
        intent.putExtra("time",articleList.get(p).getPublishedAt());
        intent.putExtra("content",articleList.get(p).getContent());
        startActivity(intent);
    }
}