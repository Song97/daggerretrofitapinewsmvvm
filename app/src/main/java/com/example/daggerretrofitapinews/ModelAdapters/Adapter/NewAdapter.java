package com.example.daggerretrofitapinews.ModelAdapters.Adapter;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.daggerretrofitapinews.ModelAdapters.MyListener;
import com.example.daggerretrofitapinews.R;
import com.example.daggerretrofitapinews.Retorfit.Entity.Article;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

public class NewAdapter extends RecyclerView.Adapter<NewAdapter.MyViewHolder> {

    private Context context;
    private List<Article> lists;
    private MyListener myLiseners;

    public NewAdapter(Context context, List<Article> lists) {
        this.context = context;
        this.lists = lists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_custom,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Article model = lists.get(position);

        String time = model.getPublishedAt();
        String[] aSp = time.split("T");
        String[] dateSp = aSp[0].split("-");
        String[] timeSp = aSp[1].split(":");
        String day="ថ្ងៃ ";
        String month= " ខែ ";
        String year=" ឆ្នាំ ";
        String h = " ម៉ោង ";
        String m = " នាទី ";
        String s = " វិនាទី ";
        String cal = day+dateSp[2]+month+dateSp[1]+year+dateSp[0]+"\n"+
                h+timeSp[0]+":"+timeSp[1]+m;

        holder.tvTitle.setText(model.getTitle());
        holder.tvTime.setText(cal);




        Glide.with(context)
                .load(model.getUrlToImage())
                .error(R.drawable.no_image)
                .into(holder.ivPicture);

        holder.icon_vert.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,holder.icon_vert, Gravity.START);
                popupMenu.getMenuInflater().inflate(R.menu.menu_vert,popupMenu.getMenu());

                popupMenu.show();
            }
        });




    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle,tvTime;
        ImageView ivPicture,icon_vert;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.news_titleInformation);
            tvTime = itemView.findViewById(R.id.news_Time);
            ivPicture = itemView.findViewById(R.id.news_image);
            icon_vert = itemView.findViewById(R.id.custom_vert);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(myLiseners !=null){
                        if(getAdapterPosition() != RecyclerView.NO_POSITION){
                            myLiseners.myClickListener(getAdapterPosition());
                        }
                    }

                }
            });
        }
    }

    public void setClickMyLisener(MyListener myListener){
        this.myLiseners = myListener;
    }
}
